#InTimeTec Messaging Queue (IMQ)#

Purpose of this Project:
This project is to implement the messaging queue system.

Project Description:
Message queueing system is asynchronous communication between two entities; pulisher and subscriber. 
The publisher is responsible for adding messages to a queue with topic associated with it. 
A subscriber is responsible for pulling the messages from the queue for a given topic.

Assumptions:
Messages are stored in a queue and it is consumed by subscriber. 
One message will be consumed by many subscribers before the expiry time of the message. 
At once one client can be connected to only one topic while they may register to multiple topics.