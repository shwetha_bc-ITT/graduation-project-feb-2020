class InsufficientInput(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'Insufficient Inputs,{0}'.format(self.message)
        else:
            return 'Insufficient Input has been raised'


class IncorrectInput(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'Incorrect Input,{0}'.format(self.message)
        else:
            return 'Incorrect Input has been raised'


class InvalidInputType(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return 'Invalid Input Type,{0}'.format(self.message)
        else:
            return 'Invalid Input Type has been raised'