from model.database_handler import DatabaseHandler
from util import constant

database_obj = DatabaseHandler()


class Registration:
    def __init__(self, socket_handler):
        response_to_send = 'Enter Username, Password, and Role (1 for publisher and 2 for subscriber) separated by space'
        socket_handler.send_response(response_to_send, constant.REGISTER)
        response = socket_handler.receive_response()
        client_input = response['data'].split()
        is_registered = self.register_client(client_input)

        if is_registered:
            response_to_send = 'Registration successful'
        else:
            response_to_send = 'Invalid input, Try again!'

        socket_handler.send_response(response_to_send, constant.REGISTER)
        socket_handler.close_socket()
        exit(0)

    def register_client(self, client_input):
        is_registered = False
        if len(client_input) == 3:
            self.client_name = client_input[0]
            password = client_input[1]
            role = client_input[2]
            self.store_client_info(password, role)
            is_registered = True
            return is_registered
        return is_registered

    def store_client_info(self, password, role):
        database_obj.add_client(self.client_name, password, role)
