import sys
sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')

import _thread
from socket_controller.socket_handler import SocketHandler
from util.thread_handler import ClientThreadHandler
from util.log_handler import LogHandler

logger = LogHandler()


class Server(SocketHandler):
    thread_count = 0

    def __init__(self):
        super().__init__()

    def accept_connection(self):
        while True:
            self.client, self.address = self.accept_socket_connection()
            print('Connected to: ' + self.address[0] + ':' + str(self.address[1]))
            self.create_thread()
        self.close_socket()

    def create_thread(self):
        _thread.start_new_thread(self.threaded_client, (self.client, self.address))
        self.thread_count += 1
        print('Client Thread Number: ' + str(self.thread_count))

    def threaded_client(self, connection, address):
        ClientThreadHandler(connection, address)


if __name__ == '__main__':
    try:
        server = Server()
        server.create_socket()
        server.bind_socket()
        server.socket_listen()
        server.accept_connection()
    except Exception as error:
        print("Connection Terminated ", error)
        logger.log_error("Connection Terminated: Error occurred while handling Sockets")
