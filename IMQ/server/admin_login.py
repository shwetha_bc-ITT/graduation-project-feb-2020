from model.database_handler import DatabaseHandler
from util import constant
import os
import socket
import errno
import configparser

config = configparser.ConfigParser()
config_file = os.path.join(os.path.dirname(__file__), 'admin_cred.ini')
config.read(config_file)
database_obj = DatabaseHandler()


class Admin:
    def __init__(self, socket_handler):
        self.socket_handler = socket_handler
        self.handle_admin()

    def handle_admin(self):
        try:
            response_to_send = 'Enter Admin Username and Password separated by space'
            self.socket_handler.send_response(response_to_send, constant.ADMIN_LOGIN)
            response = self.socket_handler.receive_response()

            admin_name, password = response['data'].split(" ")
            is_admin = self.validate_admin(admin_name, password)
            self.socket_handler.send_response(is_admin, constant.ADMIN_LOGIN)
            response = self.socket_handler.receive_response()

            if response['request_type'] == constant.CREATE_TOPIC:
                self.handle_topic_creation(response['data'])
                response_to_send = 'Topic created successfully!'
                self.socket_handler.send_response(response_to_send, constant.CREATE_TOPIC)

            elif response['request_type'] == constant.REMOVE_TOPIC:
                topic_id = self.handle_topic_deletion(response['data'])
                if topic_id:
                    response_to_send = 'Topic deleted successfully!'
                else:
                    response_to_send = 'Topic does not Exist!'
                self.socket_handler.send_response(response_to_send, constant.REMOVE_TOPIC)

            elif response['request_type'] == constant.INVALID_INPUT:
                response_to_send = 'Invalid Admin Credentials!'
                self.socket_handler.send_response(response_to_send, constant.INVALID_INPUT)

            self.socket_handler.close_socket()
            print("Connection Closed")
            exit(0)
        except socket.error as error:
            if error.errno == errno.WSAECONNRESET:
                print("Connection Closed")

    def validate_admin(self, username, password):
        admin_name = config['admin']['username']
        admin_password = config['admin']['password']
        if username == admin_name and password == admin_password:
            return True
        else:
            return False

    def handle_topic_creation(self, topic_name):
        database_obj.add_topic(topic_name)

    def handle_topic_deletion(self, topic_name):
        topic_id = database_obj.delete_topic(topic_name)
        return topic_id
