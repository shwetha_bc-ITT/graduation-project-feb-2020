from model.database_handler import DatabaseHandler
from util import constant

database_obj = DatabaseHandler()


class Login:

    def __init__(self, socket_handler):
        self.socket_handler = socket_handler
        self.handle_login()

    def validate_user(self, username, password):
        return database_obj.validate_user(username, password)

    def handle_login(self):
        try:
            response_to_send = '\nTo login please enter Username and Password separated by space:\n'
            self.socket_handler.send_response(response_to_send, constant.LOGIN)
            response = self.socket_handler.receive_response()
            client_info = response['data'].split()

            if len(client_info) == 2:
                self.client_name = client_info[0]
                password = client_info[1]
                valid = self.validate_user(self.client_name, password)

                if valid:
                    self.socket_handler.send_response(valid, constant.LOGIN)
                    if valid == 'Pub':
                        response_to_send = 'Authentication successful!\n1.Push Message\n2.Exit\n'
                    else:
                        response_to_send = 'Authentication successful!\n1.Pull Messages\n2.Exit\n'
                    self.socket_handler.send_response(response_to_send, constant.LOGIN)
                    response = self.socket_handler.receive_response()

                    if response['request_type'] == constant.SHOW_TOPICS:
                        topics = self.get_topics()
                        self.socket_handler.send_response(topics, constant.SHOW_TOPICS)

                    if response['request_type'] == constant.PUSH_REQUEST:
                        topics = self.get_topics()
                        self.socket_handler.send_response(topics, constant.SHOW_TOPICS)

                        response = self.socket_handler.receive_response()
                        was_message_saved = self.push_message(response['data'])
                        if was_message_saved:
                            response_to_send = 'Pushed message successfully!'
                        else:
                            response_to_send = 'Incorrect number of parameters were provided!'
                        self.socket_handler.send_response(response_to_send, constant.PUSH_REQUEST)
                        while True:
                            response = self.socket_handler.receive_response()
                            try:
                                if response['data'].strip() == constant.TERMINATE_CONN:
                                    response_to_send = 'End Of Queue'
                                    self.socket_handler.send_response(response_to_send, constant.PUSH_REQUEST)
                                    print('Client terminated connection!')
                                    break

                                if response['data'].split(" ")[1] != constant.TERMINATE_CONN:
                                    self.push_message(response['data'])
                                    response_to_send = 'Pushed message successfully!'
                                    self.socket_handler.send_response(response_to_send, constant.PUSH_REQUEST)
                                else:
                                    response_to_send = 'End of Message'
                                    self.socket_handler.send_response(response_to_send, constant.PUSH_REQUEST)
                            except IndexError:
                                response_to_send = 'Incorrect number of parameters were provided!'
                                self.socket_handler.send_response(response_to_send, constant.PUSH_REQUEST)

                    if response['request_type'] == constant.CREATE_TOPIC:
                        self.handle_topic_creation(response['data'])
                        response_to_send = 'Topic created successfully!'
                        self.socket_handler.send_response(response_to_send, constant.CREATE_TOPIC)

                    if response['request_type'] == constant.SUBSCRIBE:
                        self.get_subscribed_topics(self.client_name)
                        self.add_subscription(response['data'], self.client_name)
                        response_to_send = 'Subscribed successfully!'
                        self.socket_handler.send_response(response_to_send, constant.PUSH_REQUEST)

                    if response['request_type'] == constant.PULL_REQUEST:
                        topics = self.get_topics()
                        self.socket_handler.send_response(topics, constant.SHOW_TOPICS)

                        response = self.socket_handler.receive_response()
                        response_to_send = self.get_messages(response['data'])
                        if not response_to_send:
                            response_to_send = 'No New Messages!'
                        self.socket_handler.send_response(response_to_send, constant.PULL_REQUEST)

                response_to_send = 'Invalid Credentials!'
                self.socket_handler.send_response(response_to_send, constant.NO_REQUEST)
                self.socket_handler.close_socket()
                exit(0)
            else:
                response_to_send = 'Authentication unsuccessful because required parameters not received...'
                self.socket_handler.send_response(response_to_send, constant.LOGIN)
                self.socket_handler.close_socket()
                exit(0)
        except TypeError:
            print("Client Disconnected!")

    def push_message(self, message):
        if message is not None:
            data = message.split(' ', 1)
            topic_name = data[0]
            message = data[1]
            topic_id = str(database_obj.get_topic_id(topic_name))
            publisher_id = database_obj.get_client_id(self.client_name)
            message_id = database_obj.save_messages(topic_id, message, publisher_id)
            return True
        else:
            return False

    def handle_topic_creation(self, topic_name):
        database_obj.add_topic(topic_name)

    def add_subscription(self, topic_name, client_name):
        topic_id = database_obj.get_topic_id(topic_name)
        subscriber_id = database_obj.get_client_id(client_name)
        database_obj.store_subscriber_info(str(topic_id), str(subscriber_id))

    def get_messages(self, topic_name):
        # subscriber_id = str(database_obj.get_client_id(self.client_name))
        messages = database_obj.get_messages(topic_name)
        messages_to_send = ''
        if messages is not None:
            for message in messages:
                messages_to_send = messages_to_send + message[0] + '\n'
            return messages_to_send
        return False

    def get_topics(self):
        topic_list = database_obj.get_all_topics()
        if topic_list:
            return topic_list
        return False

    def get_subscribed_topics(self, subscriber_name):
        subscriber_id = database_obj.get_client_id(subscriber_name)
        topics = database_obj.get_subscribed_topics(subscriber_id)
        messages_to_send = ''
        if topics:
            for topic in topics:
                messages_to_send = messages_to_send + str(topic[0]) + "." + topic[1] + '\n'
            print(messages_to_send)
            return messages_to_send
        return False
