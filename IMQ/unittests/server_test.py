import sys

sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')
import unittest
from server.starter import Server


class Test(unittest.TestCase):
    server_obj = Server()

    def test_for_socket_creation(self):
        self.assertTrue((type(self.server_obj.create_socket())))
        self.server_obj.close_socket()

    def test_for_bind_socket(self):
        self.server_obj.create_socket()
        self.assertTrue((type(self.server_obj.bind_socket())))
        self.server_obj.close_socket()

    def test_for_socket_listening(self):
        self.server_obj.create_socket()
        self.server_obj.bind_socket()
        self.assertTrue((type(self.server_obj.socket_listen())))
        self.server_obj.close_socket()


if __name__ == '__main__':
    unittest.main()
