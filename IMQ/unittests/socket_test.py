import sys

sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')
import unittest
from server.starter import Server
from client.starter import Client


class Test(unittest.TestCase):
    client_obj = Client()
    server_obj = Server()

    def test_for_socket_creation(self):
        self.assertTrue((type(self.client_obj.create_socket())))
        self.assertTrue((type(self.server_obj.create_socket())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()

    def test_for_bind_socket(self):
        self.server_obj.create_socket()
        self.assertTrue((type(self.server_obj.bind_socket())))
        self.server_obj.close_socket()

    def test_for_socket_listening(self):
        self.server_obj.create_socket()
        self.server_obj.bind_socket()
        self.assertTrue((type(self.server_obj.socket_listen())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()

    def test_for_socket_connection_establishment(self):
        self.server_obj.create_socket()
        self.server_obj.bind_socket()
        self.server_obj.socket_listen()
        self.client_obj.create_socket()
        self.assertTrue((type(self.client_obj.connect_socket())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()

    def test_for_accepting_socket_request(self):
        self.server_obj.create_socket()
        self.server_obj.bind_socket()
        self.server_obj.socket_listen()
        self.client_obj.create_socket()
        self.client_obj.connect_socket()
        self.assertTrue((type(self.server_obj.accept_connection())))
        self.client_obj.close_socket()
        self.server_obj.close_socket()


if __name__ == '__main__':
    unittest.main()
