import sys

sys.path.append('../')

from model.database_handler import DatabaseHandler
from server.registration import Registration
from server.login import Login
from server.admin_login import Admin
from util import constant
from socket_controller.socket_handler import SocketHandler

database_obj = DatabaseHandler()


class ClientThreadHandler:

    def __init__(self, socket_connection, address):
        self.socket_handler = SocketHandler()
        self.socket_handler.set_socket(socket_connection)
        self.handle_connection()

    def handle_connection(self):
        while True:
            response_to_send = 'Welcome to the server'
            self.socket_handler.send_response(response_to_send, constant.NO_REQUEST)
            data = self.socket_handler.receive_response()
            if not data:
                break
            if data['request_type'] == constant.REGISTER:
                self.handle_registration()
            elif data['request_type'] == constant.LOGIN:
                self.handle_login()
            elif data['request_type'] == constant.ADMIN_LOGIN:
                self.handle_admin()
        self.socket_handler.close_socket()
        exit(0)

    def handle_registration(self):
        Registration(self.socket_handler)

    def handle_login(self):
        Login(self.socket_handler)

    def handle_admin(self):
        Admin(self.socket_handler)
