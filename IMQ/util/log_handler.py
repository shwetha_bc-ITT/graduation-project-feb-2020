import sys
sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')

import logging
import datetime
import time
from util import constant

log_filename = 'D:/L&C Assignment/graduation-project-feb-2020/IMQ/util/Logs/' + constant.LOG_FILE


class LogHandler:

    def __init__(self):
        logging.basicConfig(filename=log_filename, filemode='w', format='%(name)s - %(levelname)s - %(message)s')

    def log_error(self, error):
        error = self.get_current_time() + error
        logging.error(error)

    def log_warning(self, warning):
        warning = self.get_current_time() + warning
        logging.warning(warning)

    def get_current_time(self):
        timestamp = time.time()
        connection_time = datetime.datetime.fromtimestamp(
            timestamp).strftime('%Y-%m-%d::%H:%M:%S - ')
        return connection_time
