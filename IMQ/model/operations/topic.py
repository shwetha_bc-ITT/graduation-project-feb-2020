from model.database_connection import DatabaseConnection
import mysql.connector


class TopicService:

    def __init__(self):
        db = DatabaseConnection()
        self.connection, self.cursor = db.get_connection()

    def create_table(self):
        query = "CREATE TABLE IF NOT EXISTS topics (topic_id INT AUTO_INCREMENT PRIMARY KEY, topic_name VARCHAR(255) NOT NULL" \
                " UNIQUE,publisher_id INT, CONSTRAINT topics_fk_clients FOREIGN KEY (created_by) REFERENCES clients(id))"
        self.cursor.execute(query)

    def insert_into_table(self, topic):
        try:
            query = "INSERT INTO topics (topic_name) VALUES (%s)"
            value = (topic,)
            self.cursor.execute(query, value)
            self.connection.commit()
        except mysql.connector.Error as error:
            print("Something went wrong: {}".format(error))

    def select_from_table(self):
        query = "SELECT topic_name from topics LIMIT 10"
        self.cursor.execute(query)
        return self.cursor.fetchmany(10)

    def delete_from_table(self, _id):
        query = "DELETE FROM topics WHERE topic_id = '" + str(_id) + "'"
        self.cursor.execute(query)
        self.connection.commit()

    def get_id(self, topic_name):
        query = "SELECT topic_id from topics WHERE topic_name = '" + topic_name + "'"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

