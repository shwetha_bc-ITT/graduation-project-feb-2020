from model.database_connection import DatabaseConnection
import mysql.connector


class ClientService:

    def __init__(self):
        db = DatabaseConnection()
        self.connection, self.cursor = db.get_connection()
        self.create_table()

    def create_table(self):
        self.cursor.execute("CREATE TABLE IF NOT EXISTS clients(client_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, "
                            "client_name VARCHAR(200) NOT NULL, password VARCHAR(200) NOT NULL, client_role VARCHAR("
                            "200))")

    def insert_into_table(self, client_name, password, role=1):
        try:
            query = "INSERT INTO clients(client_name, password, client_role) VALUES (%s, %s, %s)"
            if role == '1':
                client_role = 'Pub'
            elif role == '2':
                client_role = 'Sub'
            value = (client_name, password, client_role)
            self.cursor.execute(query, value)
            self.connection.commit()
            row_id = self.cursor.lastrowid
            return row_id
        except mysql.connector.Error as error:
            print("Something went wrong: {}".format(error))

    def delete_from_table(self, client_id):
        query = "DELETE FROM clients WHERE client_id = " + str(client_id)
        self.cursor.execute(query)
        self.connection.commit()

    def validate_entry(self, client_name, password):
        query = "SELECT client_role from `clients` WHERE `client_name`='" + client_name + "' AND `PASSWORD` = '" + password + "'"
        self.cursor.execute(query)
        client_role = self.cursor.fetchone()
        return client_role

    def get_id(self, client_name):
        query = "SELECT client_id from clients WHERE client_name = '" + client_name + "'"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result

