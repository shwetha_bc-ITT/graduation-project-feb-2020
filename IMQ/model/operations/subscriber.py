from model.database_connection import DatabaseConnection


class SubscriberService:
    def __init__(self):
        db = DatabaseConnection()
        self.connection, self.cursor = db.get_connection()

    def create_table(self):
        query = "CREATE TABLE IF NOT EXISTS subscribers (subscriber_id INT AUTO_INCREMENT PRIMARY KEY, " \
                "topic_id INT NOT NULL, CONSTRAINT fk_topics FOREIGN KEY (topic_id) REFERENCES topics(topic_id))"
        self.cursor.execute(query)

    def insert_into_table(self, topic_id, client_id):
        query = "INSERT into `subscribers` (`topic_id`,`subscriber_id`) values('" + str(topic_id) + "','" + str(
            client_id) + "')"
        self.cursor.execute(query)
        self.connection.commit()

    def select_from_table(self, topic_id):
        query = 'SELECT subscriber_id from subscribers WHERE topic_id = ' + topic_id
        self.cursor.execute(query)
        self.connection.commit()
        subscribers_list = self.cursor.fetchone()
        if subscribers_list:
            return subscribers_list
        return False

    def get_subscribed_topics(self, subscriber_id):
        query = "SELECT topic_id, topic_name from `topics` WHERE topic_id IN (SELECT topic_id from subscribers WHERE " \
                "`subscriber_id` = '" + str(subscriber_id) + "')"
        self.cursor.execute(query)
        topics = self.cursor.fetchmany(100)
        if topics:
            return topics
        return False
