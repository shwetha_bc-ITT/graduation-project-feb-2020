from datetime import datetime
from model.database_connection import DatabaseConnection
import mysql.connector


class MessageService:

    def __init__(self):
        db = DatabaseConnection()
        self.connection, self.cursor = db.get_connection()

    def create_table(self):
        table_name = "messages"
        query = "CREATE TABLE IF NOT EXISTS " + table_name + "(message_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " \
                                                             "create_time TIMESTAMP, message VARCHAR(1024), topic_id INT" \
                                                             " NOT NULL, publisher_id INT NOT NULL)"
        self.cursor.execute(query)

    def insert_into_table(self, topic_id, row_id, message):
        try:
            query = "INSERT INTO messages (create_time, message, topic_id, publisher_id) VALUES(%s,%s,%s,%s)"
            value = (datetime.now(), message, topic_id, row_id)
            self.cursor.execute(query, value)
            self.connection.commit()
            return self.cursor.lastrowid
        except mysql.connector.Error as error:
            print("Something went wrong: {}".format(error))

    def get_messages(self, topic_id):
        query = 'SELECT `message` FROM `messages` WHERE topic_id = ' + str(topic_id)
        self.cursor.execute(query)
        message_list = self.cursor.fetchmany(100)
        return message_list

    def delete_from_table(self, row_id):
        query = "DELETE FROM messages WHERE message_id = " + str(row_id)
        self.cursor.execute(query)
        self.connection.commit()
