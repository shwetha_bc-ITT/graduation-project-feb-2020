import mysql.connector
import configparser
import os

config = configparser.ConfigParser()
config_file = os.path.join(os.path.dirname(__file__), 'db_setup.ini')
config.read(config_file)


class DatabaseConnection:
    def __init__(self):
        self.connection, self.cursor = self.get_connection()

    @staticmethod
    def get_connection():
        try:
            connection = mysql.connector.connect(host=config['database']['host'],
                                                 user=config['database']['user'],
                                                 passwd=config['database']['password'],
                                                 database=config['database']['name'])
            cursor = connection.cursor()
            return connection, cursor

        except mysql.connector.Error as error:
            print("Connection Failed {}".format(error))

    def close_connection(self):
        if self.connection.is_connected():
            self.cursor.close()
            self.connection.close()
            print("MySQL connection is closed!")
