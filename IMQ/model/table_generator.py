import sys

sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')

import os
import configparser
import mysql.connector
from model.operations import client, messages, topic, subscriber

config = configparser.ConfigParser()
config_file = os.path.join(os.path.dirname(__file__), 'db_setup.ini')
config.read(config_file)


def get_initial_connection():
    try:
        connection = mysql.connector.connect(host=config['database']['host'],
                                             user=config['database']['user'],
                                             passwd=config['database']['password'])
        cursor = connection.cursor()
        return connection, cursor
    except mysql.connector.Error as error:
        print("Connection Failed {}".format(error))


class Initiator:
    def __init__(self):
        self.connection, self.cursor = get_initial_connection()
        self.create_database(config['database']['name'])
        self.use_database(config['database']['name'])
        self.client_service = client.ClientService()
        self.message_service = messages.MessageService()
        self.topic_service = topic.TopicService()
        self.subscriber_service = subscriber.SubscriberService()
        self.generate_tables()

    def create_database(self, database_name):
        query = "CREATE DATABASE IF NOT EXISTS " + database_name
        self.cursor.execute(query)

    def use_database(self, database_name):
        query = 'USE ' + database_name
        self.cursor.execute(query)

    def generate_tables(self):
        self.client_service.create_table()
        self.message_service.create_table()
        self.topic_service.create_table()
        self.subscriber_service.create_table()
