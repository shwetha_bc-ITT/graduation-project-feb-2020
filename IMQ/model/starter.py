"""Run this file to create the Database and tables in it"""

import sys
sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')
from model.table_generator import Initiator
from model import config

if __name__ == '__main__':
    config.configure_database()
    initiator_obj = Initiator()
    initiator_obj.generate_tables()

