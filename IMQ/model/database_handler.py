from model.operations import client, messages, topic, subscriber
from model.database_connection import DatabaseConnection


class DatabaseHandler:
    def __init__(self):
        self.client_service = client.ClientService()
        self.message_service = messages.MessageService()
        self.topic_service = topic.TopicService()
        self.subscriber_service = subscriber.SubscriberService()
        self.connection, self.cursor = DatabaseConnection().get_connection()
        self.row_id = -1

    def add_client(self, client_name, password, role):
        self.row_id = self.client_service.insert_into_table(client_name, password, role)

    def store_message_info(self, message_data, topic_name):
        self.topic_id = self.topic_service.get_id(topic_name)
        messages.MessageService().insert_into_table(self.row_id, message_data, self.topic_id)

    def add_topic(self, topic_name):
        self.topic_service.insert_into_table(topic_name)

    def delete_topic(self, topic_name):
        topic_id = self.topic_service.get_id(topic_name)
        if topic_id is not None:
            self.topic_service.delete_from_table(topic_id[0])
            return topic_id[0]
        else:
            return False

    def validate_user(self, username, password):
        role = self.client_service.validate_entry(username, password)
        if role is not None:
            return role[0]
        return False

    def store_subscriber_info(self, topic_id, client_id):
        self.subscriber_service.insert_into_table(topic_id, client_id)

    def get_subscribers_for_topic(self, topic_id):
        self.subscriber_service.select_from_table(topic_id)

    def get_topic_id(self, topic_name):
        topic_id = self.topic_service.get_id(topic_name)
        return topic_id[0]

    def save_messages(self, topic_id, message, publisher_id):
        message_id = self.message_service.insert_into_table(topic_id, publisher_id, message)
        return message_id

    def get_all_topics(self):
        topic_tuple_list = self.topic_service.select_from_table()
        topic_list = [topic_tuple[0] for topic_tuple in topic_tuple_list]
        return topic_list

    def get_subscribed_topics(self, subscriber_id):
        topics = self.subscriber_service.get_subscribed_topics(subscriber_id)
        return topics

    def get_messages(self, topic_name):
        topic_id = self.get_topic_id(topic_name)
        message_list = self.message_service.get_messages(topic_id)
        if message_list:
            return message_list
        return False

    def get_client_id(self, client_name):
        client_id = self.client_service.get_id(client_name)
        if client_id:
            return client_id[0]
        return False
