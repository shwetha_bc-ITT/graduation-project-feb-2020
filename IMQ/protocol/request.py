from protocol.message_protocol import Protocol
from dataclasses import dataclass


@dataclass
class Request(Protocol):
    requestType: str = "GET"
