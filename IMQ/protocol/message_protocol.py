from dataclasses import dataclass


@dataclass
class Protocol:
    data: str
    request_type: int
    version: int = 1
    format: str = "json"
