import jsonpickle
import json


class Parser:
    @staticmethod
    def json_to_pickle(json_data):
        return json.dumps(jsonpickle.encode(json_data, unpicklable=False), indent=4)

    @staticmethod
    def pickle_to_json(pickled_json):
        json_data = json.loads(pickled_json)
        return json.loads(json_data)
