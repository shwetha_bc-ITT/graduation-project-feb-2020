from protocol.message_protocol import Protocol
from dataclasses import dataclass


@dataclass
class Response(Protocol):
    requestType: str = "POST"
