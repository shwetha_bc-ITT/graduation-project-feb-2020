import sys
sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')
import socket
from protocol.parser import Parser
from protocol.request import Request
from util import constant
from util.log_handler import LogHandler

logger = LogHandler()


class SocketHandler:

    def __init__(self):
        self.host = socket.gethostname()
        self.port = constant.PORT

    def set_socket(self,socket_connection):
        self.socket_obj = socket_connection

    def create_socket(self):
        try:
            self.socket_obj = socket.socket()
            return True
        except socket.error as error:
            logger.log_error(error)
            print("Error while creating socket!", error)
            return False

    def bind_socket(self):
        try:
            self.socket_obj.bind((self.host, self.port))
            return True
        except socket.error as error:
            logger.log_error(str(error))
            print(str(error))
            self.socket_obj.close()

    def socket_listen(self):
        try:
            print('Waiting for a Connection..')
            self.socket_obj.listen(constant.MAX_CONNECTION)
            return True
        except socket.error as error:
            logger.log_error(str(error))
            print(str(error))
            self.socket_obj.close()

    def connect_socket(self):
        try:
            print('Waiting for connection...')
            self.socket_obj.connect((self.host, self.port))
            print('Connected..!!')
            return True
        except socket.error as error:
            logger.log_error(error)
            print(error)
            self.socket_obj.close()

    def accept_socket_connection(self):
        try:
            client, address = self.socket_obj.accept()
            return client, address
            return True
        except socket.error as error:
            logger.log_error(str(error))
            print(error)
            self.socket_obj.close()

    def close_socket(self):
        try:
            self.socket_obj.close()
            return True
        except socket.error as error:
            logger.log_error(str(error))
            print(error)
            self.socket_obj.close()

    def receive_response(self):
        try:
            data = self.socket_obj.recv(constant.MAX_BYTES)
            if not data:
                return False
            parsed_response = Parser.pickle_to_json(data)
            return parsed_response
        except socket.error as error:
            logger.log_error(str(error))
            print(error)
            self.socket_obj.close()

    def send_response(self, request, _type):
        try:
            response_header = Request(request, _type)
            parsed_header = Parser.json_to_pickle(response_header)
            self.socket_obj.send(str.encode(parsed_header))
            return True
        except socket.error as error:
            logger.log_error(str(error))
            print(error)
            self.socket_obj.close()
