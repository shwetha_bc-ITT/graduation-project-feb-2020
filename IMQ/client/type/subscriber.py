from util import constant
from socket_controller.socket_handler import SocketHandler
from exceptions.custom_exceptions import IncorrectInput


class Subscriber:

    def __init__(self, socket_connection):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket_connection
        self.handle_subscriber()

    def handle_subscriber(self):
        try:
            print("You are --Subscriber--")
            response = self.socket_handler.receive_response()
            choice = input(response['data'])
            if 1 <= int(choice) <= 3:
                if choice == '1':
                    self.pull_message()
                elif choice == '2':
                    pass
                elif choice == '3':
                    self.subscribe_to_topic()
            else:
                raise IncorrectInput("Invalid Number Entered")

        except ValueError:
            print("Value entered not a Number!")

        except IncorrectInput:
            print("Value entered is Incorrect!")

    def subscribe_to_topic(self):
        topic_name = str(input("Please enter topic name:\n")).strip()
        self.socket_handler.send_response(topic_name, constant.SUBSCRIBE)
        response = self.socket_handler.receive_response()
        print(response['data'])

    def pull_message(self):
        self.socket_handler.send_response('', constant.PULL_REQUEST)
        response = self.socket_handler.receive_response()
        print("\nAvailable topics are:\n", response['data'])

        topic_name = str(input("Please enter topic name to pull messages:\n")).strip()
        self.socket_handler.send_response(topic_name, constant.PULL_REQUEST)
        response = self.socket_handler.receive_response()
        print("Messages in topic '{}' are as follows:\n".format(topic_name), response['data'])
