import sys

sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')

from util import constant
from socket_controller.socket_handler import SocketHandler
from exceptions.custom_exceptions import InvalidInputType, InsufficientInput, IncorrectInput


class Publisher:

    def __init__(self, socket):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        self.handle_publisher()

    def handle_publisher(self):
        try:
            print("You are --Publisher--")
            response = self.socket_handler.receive_response()
            choice = input(response['data'])
            if choice is None:
                raise InvalidInputType("No Value Entered!")
            if 1 <= int(choice) <= 2:
                if choice == '1':
                    self.push_message()
                elif choice == '2':
                    pass
            else:
                raise IncorrectInput("Invalid Number Entered")

        except ValueError:
            print("Value Entered Not A Number!")

        except IncorrectInput:
            print("Value Entered Is Incorrect!")

    def push_message(self):
        self.socket_handler.send_response('', constant.PUSH_REQUEST)
        response = self.socket_handler.receive_response()
        print("\nAvailable topics are:\n", response['data'])

        topic_name = input("Please enter topic name and push message:\n")
        self.socket_handler.send_response(topic_name, constant.PUSH_REQUEST)
        response = self.socket_handler.receive_response()
        print(response['data'])
        while response['data'] != 'End Of Queue':
            topic_name = input("Please enter topic name and push message:\n")
            self.socket_handler.send_response(topic_name, constant.PUSH_REQUEST)
            response = self.socket_handler.receive_response()
            print(response['data'])
