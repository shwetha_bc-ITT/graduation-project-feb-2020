import sys
sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')

from util import constant
from socket_controller.socket_handler import SocketHandler
from exceptions.custom_exceptions import InvalidInputType, IncorrectInput


class Admin:

    def __init__(self, socket, creds=True):
        self.socket_handler = SocketHandler()
        self.socket_handler.socket_obj = socket
        if creds:
            self.handle_admin()
        else:
            self.handle_invalid_login()

    def handle_admin(self):
        try:
            choice = input("Enter '1' to Create Topic and '2' to Delete Topic:\n")
            if choice is None:
                raise InvalidInputType("No Value Entered!")
            elif int(choice) == 1:
                self.create_topic()
            elif int(choice) == 2:
                self.delete_topic()
            else:
                raise IncorrectInput("Invalid Number Entered")

        except ValueError:
            print("Value Entered Not A Number!")

        except IncorrectInput:
            print("Value Entered Is Incorrect!")

    def handle_invalid_login(self):
        self.socket_handler.send_response('', constant.INVALID_INPUT)
        response = self.socket_handler.receive_response()
        print(response['data'])

    def create_topic(self):
        topic_name = input("Please enter topic name:\n")
        self.socket_handler.send_response(topic_name, constant.CREATE_TOPIC)
        response = self.socket_handler.receive_response()
        print(response['data'])

    def delete_topic(self):
        topic_name = input("Please enter topic name:\n")
        self.socket_handler.send_response(topic_name, constant.REMOVE_TOPIC)
        response = self.socket_handler.receive_response()
        print(response['data'])

