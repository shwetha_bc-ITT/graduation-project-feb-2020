import sys
sys.path.append('D:/L&C Assignment/graduation-project-feb-2020/IMQ/')

from socket_controller.socket_handler import SocketHandler
from util import constant
import socket
import errno
from client.type.publisher import Publisher
from client.type.subscriber import Subscriber
from client.type.admin import Admin
from exceptions.custom_exceptions import InvalidInputType, InsufficientInput, IncorrectInput
from util.log_handler import LogHandler

logger = LogHandler()


class Client(SocketHandler):

    def __init__(self):
        super().__init__()

    def read_choice(self):
        try:
            request = input("Enter '1' to Register, '2' to Login and '3' for Admin Login:\n")
            if int(request) > 3 or int(request) < 1:
                raise InvalidInputType
            if int(request) == constant.REGISTER:
                self.send_response(request, constant.REGISTER)
                self.handle_registration()
            elif int(request) == constant.LOGIN:
                self.send_response(request, constant.LOGIN)
                self.handle_login()
            elif int(request) == constant.ADMIN_LOGIN:
                self.send_response(request, constant.ADMIN_LOGIN)
                self.handle_admin()

        except ValueError:
            print("Value Entered Was Not A Number")
        except InvalidInputType:
            print("Value Entered Was Incorrect")

    def read_response(self):
        response = self.receive_response()
        print('Response from server:\n' + response['data'])
        print()
        self.read_choice()
        self.close_socket()

    def handle_login(self):
        try:
            response = self.receive_response()
            client_info = input(response['data'])

            if client_info is None:
                raise InvalidInputType("No Value Entered!")
            if len(client_info.split(' ')) < 2:
                raise InsufficientInput("User Input inadequate")
            elif len(client_info.split(' ')) > 2:
                raise InvalidInputType("Exceeded maximum number of arguments")

            self.send_response(client_info, constant.LOGIN)
            response = self.receive_response()

            if response['request_type'] == constant.LOGIN:
                role = response['data']
                if role == 'Pub':
                    Publisher(self.socket_obj)
                elif role == 'Sub':
                    Subscriber(self.socket_obj)
            # Invalid login credentials
            elif response['request_type'] == constant.NO_REQUEST:
                print(response['data'])

        except InvalidInputType:
            print("Exceeded Two arguments!")

        except InsufficientInput:
            print("Password not entered!")

    def handle_admin(self):
        try:
            """Input for Username and password"""
            response = self.receive_response()
            admin_info = str(input('Response from server:\n' + response['data'] + '\n')).strip()

            self.send_response(admin_info, constant.ADMIN_LOGIN)

            is_admin = self.receive_response()

            if admin_info is not None:
                if len(admin_info.split(' ')) < 2:
                    raise InsufficientInput("User Input inadequate")
                elif len(admin_info.split(' ')) > 2:
                    raise IncorrectInput("Exceeded maximum number of arguments")

                if is_admin['data']:
                    Admin(self.socket_obj)
                else:
                    Admin(self.socket_obj, creds=False)
            else:
                raise InvalidInputType("No Value Entered!")

        except IncorrectInput:
            print("Exceeded Three arguments!")

        except InsufficientInput:
            print("Password/Role not entered!")

        except InvalidInputType:
            print("No input received..")

    def handle_registration(self):
        try:
            response = self.receive_response()
            client_info = input('Response from server:\n' + response['data'] + '\n')

            if client_info is None:
                raise InvalidInputType("No Value Entered!")
            if len(client_info.split(' ')) < 3:
                raise InsufficientInput("User Input inadequate")
            elif len(client_info.split(' ')) > 3:
                raise IncorrectInput("Exceeded maximum number of arguments")

            self.send_response(client_info, constant.REGISTER)
            response = self.receive_response()
            print('Response from server:\n' + response['data'] + '\n')

        except IncorrectInput:
            print("Exceeded Three arguments!")

        except InsufficientInput:
            print("Password/Role not entered!")

        except InvalidInputType:
            print("No input received..")


if __name__ == '__main__':
    try:
        client = Client()
        client.create_socket()
        client.connect_socket()
        client.read_response()

    except socket.error as error:
        if error.errno == errno.WSAECONNRESET:
            print("Connection Closed")

    except Exception as error:
        print("Connection Terminated ", error)
        logger.log_error("Connection Terminated")
